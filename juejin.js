// ==UserScript==
// @name         掘金阅读优化
// @namespace    http://javeer.cn/
// @version      0.1
// @description  掘金阅读优化
// @author       cn-src
// @match        https://juejin.cn/post/*
// @grant        none
// ==/UserScript==

(function () {
    'use strict';
    var sidebarContainer = document.querySelector('#sidebar-container');

    // 使用 remove 方法直接删除元素
    if (sidebarContainer) {
        sidebarContainer.remove();
    } else {
        console.log("未找到 id 为 sidebar-container 的元素。");
    }

    var mainElement = document.getElementsByTagName('main')[0];

    if (mainElement) {
        mainElement.style.width = '100%';
    }

    var mainAreaElement = document.getElementsByClassName('main-area')[0];

    if (mainAreaElement) {
        mainAreaElement.style.width = '100%';
    }

})();