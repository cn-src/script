set nocompatible          " 不使用vi的键盘模式
filetype off

" 插件管理器
call plug#begin()
Plug 'dag/vim-fish'
Plug 'blueshirts/darcula'
call plug#end()

filetype plugin indent on

" 修复 Tmux 下颜色问题
if exists('$TMUX')
  set term=screen-256color
endif

syntax on                 " 自动语法高亮
set hls                   " 设置搜索高亮
set incsearch             " 设置增量搜索
set noeb                  " 禁用提示音
set vb t_vb=
set nu                    " 显示行号
set noexpandtab           " 不使用空格代替制表符
set shiftwidth=4          " 默认缩进4个空格
set softtabstop=4         " 将4个空格作为一个整体操作
set tabstop=4             " 制表符宽度4
set nobackup              " 不使用备份
set noswapfile            " 关闭交换文件
set autoread              " 文件在vim外修改过，自动重载
set textwidth=80          " 自动换行时的字符宽度
set nowrap                " 不自动换行
set fenc=utf-8            " 设定默认解码
set fencs=utf-8,usc-bom,euc-jp,gb18030,gbk,gb2312,cp936
set linespace=20          " 字符间插入的像素行数目
set langmenu=zh_CN.UTF-8  " 语言设置
" set paste                 " 粘贴时不自动处理格式

" 显示中文帮助
if version >= 603
    set helplang=cn
    set encoding=utf-8
endif

" colorscheme darcula

map <F2> :NERDTreeToggle<CR>