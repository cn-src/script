# 脚本

## 初始化 Systemd 服务
```shell
curl 'https://gitee.com/cn-src/script/raw/main/systemd-service' | bash -s xxx.jar xxx-service
```
## 初始化系统环境
```shell
curl 'https://gitee.com/cn-src/script/raw/main/init-env' | bash -s dev
```